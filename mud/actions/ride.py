# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import RideEvent, RideOffEvent

class RideAction(Action2):
    EVENT = RideEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "ride"
class RideOffAction(Action2):
    EVENT = RideOffEvent
    ACTION = "ride-off"
    RESOLVE_OBJECT = "resolve_for_use"