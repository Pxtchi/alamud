# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class RideEvent(Event2):
    NAME = "ride"

    def perform(self):
        if not self.object.has_prop("ridable"):
            self.add_prop("object-not-ridable")
            return self.take_failed()
        self.inform("ride")
        
class RideOffEvent(Event2):
    NAME = "ride-off"

    def perform(self):
        if not self.object.has_prop("ridable"):
            self.fail()
            return self.inform("ride-off.failed")
        self.inform("ride-off")